﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that gives the shield the bash mechanic.
/// </summary>
public class Shield : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;

    Vector3 initialPos;
	Vector3 sourcePoint;
	Vector3 currentPos;

	float endPosRight = 0.3f;
	float endPosLeft = -1.12f;
	private readonly float bashSpeed = 7.0f;
	private readonly float distance = 1.0f;

	public bool inputBash = false;
	public bool bashReady = true;
    private bool isBashing = false;

	PlayerMovement playerMovement;

	GameObject mage;

    string command;

    public float bashRate = 2.0f;
    private static float bashShot = 0.0f;

//    AnalyticsManager analytics;

    [SerializeField]
    GameObject shieldAnimatorObject;
    Animator shieldAnimator;

    [SerializeField]
    AudioClip bashSound;

    private AudioSource source;

    private readonly float volHigh = 0.3f;
    private readonly float volLow = 0.2f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    void Start()
	{
		gameObject.tag = "Shield";

		initialPos = transform.localPosition;
		endPosRight = transform.localPosition.x + endPosRight;
		endPosLeft = transform.localPosition.x + endPosLeft;
		sourcePoint = new Vector3 (0, 0, 0);

		playerMovement = GetComponentInParent<PlayerMovement>();

		mage = GameObject.Find("Mage");

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        shieldAnimatorObject = GameObject.Find("ShieldCooldown");
        shieldAnimator = shieldAnimatorObject.GetComponent<Animator>();

        if (check.player2_PS4)
        {
            command = "Fire3_P2_PS4";
        }
        else if (check.player2_XBox)
        {
            command = "Fire3_P2_XBox";
        }
        else
        {
            command = "Fire3_P2_Keyboard";
        }

//        analytics = stats.GetComponent<AnalyticsManager>();
    }

    void Update()
	{
		if (Input.GetButtonDown(command) && Time.time > bashShot)
		{
            bashShot = Time.time + bashRate;

            gameObject.tag = "ShieldPush";

			if (playerMovement.facingRight)
			{
				if (bashReady)
				{
					initialPos = transform.localPosition;
					inputBash = true;

                    if (!shieldAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                    {
                        shieldAnimator.SetTrigger("Active");
                    }

                    StartCoroutine("BashRight");
				}
			}
			else
			{
				if (bashReady)
				{
					initialPos = transform.localPosition;
					inputBash = true;

                    if (!shieldAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                    {
                        shieldAnimator.SetTrigger("Active");
                    }


                    StartCoroutine("BashLeft");
				}
			}

//            analytics.bashAmount++;
		}

		if (transform.localPosition.x == initialPos.x)
		{
			bashReady = true;
		}
		else
		{
			bashReady = false;
		}

		currentPos = transform.localPosition;
	}

	public void RemoveShield()
	{
		Destroy(gameObject);
	}

	IEnumerator BashRight()
	{
		while (true)
		{
			//Prevents the warrior from bashing when bashing in progress
			if (transform.localPosition.x >= endPosRight)
			{
				inputBash = false;
			}
			if (inputBash)
			{
				transform.Translate(Vector3.right * bashSpeed * Time.smoothDeltaTime);
                isBashing = true;
				
			}
			if (distance <= sourcePoint.x - currentPos.x)
			{
				GetComponentInParent<ShieldUp>().ShieldGone();
				Destroy(gameObject);
			}
			//Returns the shield to the original position after bashing
			else if (!inputBash)
			{
				gameObject.tag = "Shield";
				//mage.GetComponent<KnockBack>().NoBash();
				transform.Translate(Vector3.left * bashSpeed * Time.smoothDeltaTime);
				if (transform.localPosition.x < initialPos.x)
				{
					transform.localPosition = initialPos;
                    isBashing = false;
					StopAllCoroutines();
				}
			}

			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator BashLeft()
	{
		while (true)
		{
			//Prevents the warrior from bashing when bashing in progress
			if (transform.localPosition.x >= endPosRight)
			{
				inputBash = false;
			}
			if (inputBash)
			{
				transform.Translate(Vector3.left * bashSpeed * Time.smoothDeltaTime);
                isBashing = true;
            }
            if (distance <= sourcePoint.x - currentPos.x)
			{
				GetComponentInParent<ShieldUp>().ShieldGone();
				Destroy(gameObject);
			}

			//Returns the shield to the original position after bashing
			else if (!inputBash)
			{
				gameObject.tag = "Shield";
				//mage.GetComponent<KnockBack>().NoBash();
				transform.Translate(Vector3.right * bashSpeed * Time.smoothDeltaTime);
				if (transform.localPosition.x > initialPos.x)
				{
					transform.localPosition = initialPos;
                    isBashing = false;
					StopAllCoroutines();
				}
			}

			yield return new WaitForEndOfFrame();
		}
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        float vol = Random.Range(volLow, volHigh);

        if (collision.gameObject.tag == "Enemy")
        {
            GetComponentInParent<Health>().GiveBack();
        }

        if (isBashing)
        {
            source.PlayOneShot(bashSound, vol);
        }
    }
}