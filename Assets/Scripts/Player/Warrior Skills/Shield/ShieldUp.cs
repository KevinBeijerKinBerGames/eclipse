﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// Script that gives the warrior a shield he can use.
/// </summary>
public class ShieldUp : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;
    string sideCommand, upCommand;

	public GameObject shield;
	public GameObject shieldPlatform;
	public Transform shieldSpawn;
	public Transform shieldAbove;

	PlayerMovement playerMovement;
	
	bool shieldUp = false;
    bool shieldFront = false;

	bool shieldPresent = false;

    void Start ()
	{
		playerMovement = GetComponent<PlayerMovement>();

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        if (check.player2_PS4)
        {
            upCommand = "Fire1_P2_PS4";
            sideCommand = "Fire2_P2_PS4";
        }
        else if (check.player2_XBox)
        {
            upCommand = "Fire1_P2_XBox";
            sideCommand = "Fire2_P2_XBox";
        }
        else
        {
            upCommand = "Fire1_P2_Keyboard";
            sideCommand = "Fire2_P2_Keyboard";
        }
    }

    void Update ()
	{
        if (Input.GetButtonDown(sideCommand) && shieldUp == false && shieldFront == false)
        {
            Block(shield);
            shieldFront = true;
        }

        if (Input.GetButtonUp(sideCommand) && shieldFront == true)
        {
            ShieldRemove();
            shieldFront = false;
        }

        if (Input.GetButtonDown(upCommand) && shieldUp == false && shieldFront == false)
        {
            Platform(shieldPlatform);
            shieldUp = true;
        }

        if (Input.GetButtonUp(upCommand) && shieldUp == true)
        {
            PlatformRemove();
            shieldUp = false;
        }
    }

	void Block(GameObject blocker)
	{
        playerMovement.shieldUp = true;

		GameObject go;

		if (playerMovement.facingRight)
		{
			shieldSpawn.rotation = new Quaternion(0, 0, 0, 0);
			go = Instantiate(blocker, shieldSpawn.position, shieldSpawn.rotation);
			shieldPresent = true;
		}
		else
		{
			shieldSpawn.rotation = new Quaternion(0, 180, 0, 0);
			go = Instantiate(blocker, shieldSpawn.position, shieldSpawn.rotation);
			shieldPresent = true;
		}

		go.transform.SetParent(transform);
	}

	void Platform(GameObject platform)
	{
        playerMovement.shieldUp = true;

        GameObject go;

		if (playerMovement.facingRight)
		{
			shieldAbove.rotation = new Quaternion(0, 0, 0, 0);
			go = Instantiate(platform, shieldAbove.position, shieldAbove.rotation);
			shieldPresent = true;
		}
		else
		{
			shieldAbove.rotation = new Quaternion(0, 180, 0, 0);
			go = Instantiate(platform, shieldAbove.position, shieldAbove.rotation);
			shieldPresent = true;
		}

		go.transform.SetParent(transform);
	}

	private void ShieldRemove()
	{
		if (shieldPresent == true)
		{
            playerMovement.shieldUp = false;

            GetComponentInChildren<Shield>().RemoveShield();
		}
	}

	private void PlatformRemove()
	{
		if (shieldPresent == true)
		{
            playerMovement.shieldUp = false;

            GetComponentInChildren<ShieldLaunch>().RemoveShield();
		}
	}

	public void ShieldGone()
	{
		shieldPresent = false;
	}
}