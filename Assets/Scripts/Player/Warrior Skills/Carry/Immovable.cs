﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes objects carriable.
/// </summary>
public class Immovable : MonoBehaviour
{
	public bool beingCarried;
	float xPos;

	void Start()
	{
		xPos = transform.position.x;
	}

	void Update()
	{
		if (beingCarried == false)
		{
			transform.position = new Vector2(xPos, transform.position.y);
		}
		else
		{
			xPos = transform.position.x;
		}
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            beingCarried = false;
        }
    }
}