﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gives the warrior the ability to carry and walk with objects.
/// </summary>
public class CarryObject : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;

    public float distance = 1.0f;
    public float throwForce;

    bool holdingObject;

    RaycastHit2D hit;

    GameObject character;

    public LayerMask notGrab;

    public Transform grabSpot;

    string command;

    private void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        if (check.player2_PS4)
        {
            command = "Fire2_P2_PS4";
        }
        else if (check.player2_XBox)
        {
            command = "Fire2_P2_XBox";
        }
        else
        {
            command = "Fire2_P2_Keyboard";
        }
    }

    void Update()
    {
        //Input to Pick up stuff
        if (Input.GetButtonDown(command))
        {
            if (!holdingObject)
            {
                Physics2D.queriesStartInColliders = false;

                hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distance);

                if (hit.collider != null && hit.collider.gameObject.tag == "Obstacle")
                {
                    holdingObject = true;
                }
            }
            else
            {
                holdingObject = false;

                if (hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
                {

                    hit.collider.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x, 1) * throwForce;
                }
            }
        }

        if (holdingObject)
        {
            hit.collider.gameObject.GetComponent<Immovable>().beingCarried = true;
            hit.collider.gameObject.transform.position = grabSpot.position;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);
    }
}