﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlockBehavior : MonoBehaviour
{
	float destroyTime;
	float fallTime;
	public float fallDamageTime = 0.5f;

    public AudioClip landSound;

    private AudioSource source;

    private readonly float volHigh = 0.3f;
    private readonly float volLow = 0.2f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    void Start ()
	{
		destroyTime = 0.0f;
		fallTime = 0.0f;
		gameObject.tag = "IceBlock-Spawn";
	}

	void Update()
	{
		destroyTime += Time.deltaTime;

		if (destroyTime >= 6.0f)
		{
			Destroy(gameObject);
		}

		if (gameObject.tag == "IceBlock-Spawn" && gameObject.tag != "IceBlock-Ground")
		{
			fallTime += Time.deltaTime;

			if (fallTime >= fallDamageTime)
			{
				gameObject.tag = "IceBlock-Fall";
			}
		}

		if (gameObject.tag == "IceBlock-Ground")
		{
			fallTime = 0.0f;
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
        float vol = Random.Range(volLow, volHigh);

        if (collision.gameObject.layer == 8)
		{
			gameObject.tag = "IceBlock-Ground";
		}

		if (gameObject.tag == "IceBlock-Spawn")
		{
			if (collision.gameObject.tag == "Player")
			{
				Destroy(gameObject);
			}
		}

		if (collision.gameObject.tag == "Enemy")
		{
			Destroy(gameObject);
		}

        if (collision.gameObject.tag == "FireBall" || collision.gameObject.tag == "Pull" || collision.gameObject.tag == "Push")
        {
            Destroy(gameObject);
        }

        source.PlayOneShot(landSound, vol);
    }

	void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.layer == 8 || collision.gameObject.tag == "ShieldPlatform")
		{
			gameObject.tag = "IceBlock-Spawn";
		}
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.layer == 8 || collision.gameObject.tag == "ShieldPlatform")
		{
			gameObject.tag = "IceBlock-Ground";
		}
	}

	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.layer == 8 || collision.gameObject.tag == "ShieldPlatform")
		{
			gameObject.tag = "IceBlock-Spawn";
		}
	}
}