﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldLaunch : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;
    string command;

    Vector3 initialPos;

	float endPos = 0.4f;

	private readonly float bashSpeed = 6.5f;

	public bool inputBash = false;
	public bool bashReady = true;

	GameObject mage;

    public float bashRate = 2.0f;
    private static float bashShot = 0.0f;

//    AnalyticsManager analytics;

    [SerializeField]
    GameObject shieldAnimatorObject;
    Animator shieldAnimator;

    void Start()
	{
		initialPos = transform.localPosition;
		endPos = transform.localPosition.y + endPos;

		mage = GameObject.Find("Mage");

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        shieldAnimatorObject = GameObject.Find("PlatformCooldown");
        shieldAnimator = shieldAnimatorObject.GetComponent<Animator>();

        if (check.player2_PS4)
        {
            command = "Fire3_P2_PS4";
        }
        else if (check.player2_XBox)
        {
            command = "Fire3_P2_XBox";
        }
        else
        {
            command = "Fire3_P2_Keyboard";
        }

//        analytics = stats.GetComponent<AnalyticsManager>();
    }
    
    void Update ()
	{
		if (Input.GetButtonDown(command) && Time.time > bashShot)
		{
			if (bashReady)
			{
                bashShot = Time.time + bashRate;

                initialPos = transform.localPosition;
				inputBash = true;

                if (!shieldAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                {
                    shieldAnimator.SetTrigger("Active");
                }

                StartCoroutine("BashUp");

 //               analytics.launchAmount++;
            }
		}

		if (transform.localPosition.y == initialPos.y)
		{
			bashReady = true;
		}
		else
		{
			bashReady = false;
		}
	}

	public void RemoveShield()
	{
		Destroy(gameObject);
	}

	IEnumerator BashUp()
	{
		while (true)
		{
			//Prevents the warrior from bashing when bashing in progress
			if (transform.localPosition.y >= endPos)
			{
				inputBash = false;
			}
			if (inputBash)
			{
				gameObject.tag = "Launching";
				//GetComponentInChildren<TagSwitcher>().TagToLaunching();
				transform.Translate(Vector3.up * bashSpeed * Time.smoothDeltaTime);

			}
			//Returns the shield to the original position after bashing
			else if (!inputBash)
			{
				//mage.GetComponent<KnockBack>().NoBash();
				transform.Translate(Vector3.down * bashSpeed * Time.smoothDeltaTime);
				if (transform.localPosition.y < initialPos.y)
				{
					transform.localPosition = initialPos;
					gameObject.tag = "ShieldPlatform";
					//GetComponentInChildren<TagSwitcher>().TagToPlatform();
					StopAllCoroutines();
				}
			}

			yield return new WaitForEndOfFrame();
		}
	}
}