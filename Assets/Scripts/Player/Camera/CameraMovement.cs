﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that keeps both player characters in the screen.
/// </summary>
public class CameraMovement : MonoBehaviour
{
    Camera cam;

    public Transform player1;
    public Transform player2;

    public float minSizeY = 5f;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        //Calls the functinos below.
        SetCameraPos();
        SetCameraSize();
    }

    //Function to find the center of the 2 players
    void SetCameraPos()
    {
        Vector3 middle = (player1.position + player2.position) * 0.5f;

        cam.transform.position = new Vector3(middle.x, middle.y, cam.transform.position.z);
    }

    //Function to set the size of the camera (when the players go far apart)
    void SetCameraSize()
    {
        float minSizeX = minSizeY * Screen.width / Screen.height;

        float width = Mathf.Abs(player1.position.x - player2.position.x) * 0.5f;
        float height = Mathf.Abs(player1.position.y - player2.position.y) * 0.5f;

        float camSizeX = Mathf.Max(width, minSizeX);
        cam.orthographicSize = Mathf.Max(height, camSizeX * Screen.height / Screen.width, minSizeY);
    }
}