﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Movement for the individual player movements.
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;

    [SerializeField]
    int player = 1;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 2500f;

    string horizontal, jump;

    private Transform groundCheck;
    private bool grounded, jumpable, onIce, canMoveLeft, canMoveRight, canMoveUp;
    public bool facingRight, boost, shieldUp;
    private Animator anim;

    [SerializeField]
    AudioClip jumpSound;

    AudioSource source;

    public Vector3 respawnPoint;

    Vector3 respawnOffset;

    void Awake()
    {
        groundCheck = transform.GetChild(0);
        anim = GetComponent<Animator>();

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        source = GetComponent<AudioSource>();
    }

    void Start()
    {
        grounded = jumpable = boost = false;
        facingRight = canMoveLeft = canMoveRight = canMoveUp = true;
        if (player == 1)
        {
            if (check.player1_PS4)
            {
                horizontal = "Horizontal_P1_Controller";
                jump = "Jump_P1_PS4";
            }
            else if (check.player1_XBox)
            {
                horizontal = "Horizontal_P1_Controller";
                jump = "Jump_P1_XBox";
            }
            else
            {
                horizontal = "Horizontal_P1_Keyboard";
                jump = "Jump_P1_Keyboard";
            }
        }
        if (player == 2)
        {
            if (check.player2_PS4)
            {
                horizontal = "Horizontal_P2_Controller";
                jump = "Jump_P2_PS4";
            }
            else if (check.player2_XBox)
            {
                horizontal = "Horizontal_P2_Controller";
                jump = "Jump_P2_XBox";
            }
            else
            {
                horizontal = "Horizontal_P2_Keyboard";
                jump = "Jump_P2_Keyboard";
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            transform.position = respawnPoint;
        }

        /* if (check.overRide)
         {
             if (player == 1)
             {
                 if (check.player1_PS4)
                 {
                     horizontal = "Horizontal_P1_Controller";
                     jump = "Jump_P1_PS4";
                 }
                 else if (check.player1_XBox)
                 {
                     horizontal = "Horizontal_P1_Controller";
                     jump = "Jump_P1_XBox";
                 }
                 else
                 {
                     horizontal = "Horizontal_P1_Keyboard";
                     jump = "Jump_P1_Keyboard";
                 }
             }
             if (player == 2)
             {
                 if (check.player2_PS4)
                 {
                     horizontal = "Horizontal_P2_Controller";
                     jump = "Jump_P2_PS4";
                 }
                 else if (check.player2_XBox)
                 {
                     horizontal = "Horizontal_P2_Controller";
                     jump = "Jump_P2_XBox";
                 }
                 else
                 {
                     horizontal = "Horizontal_P2_Keyboard";
                     jump = "Jump_P2_Keyboard";
                 }
             }
         }
         */
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (name == "Mage")
        {
            if ((Input.GetButtonDown(jump) && grounded && canMoveUp) || (Input.GetButtonDown(jump) && boost && canMoveUp))
            {
                jumpable = true;
                boost = false;
            }
        }

        if (name == "Warrior")
        {
            if ((Input.GetButtonDown(jump) && (grounded || onIce == true) && canMoveUp) || (Input.GetButtonDown(jump) && boost && canMoveUp))
            {
                jumpable = true;
                boost = false;
            }

            if (shieldUp)
            {
                anim.SetBool("ShieldUp", true);
            }
            else
            {
                anim.SetBool("ShieldUp", false);
            }
        }
    }

    void FixedUpdate()
    {
        // Cache the horizontal input.
        float h = Input.GetAxis(horizontal);

        if (h != 0)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }

        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            if (canMoveLeft && canMoveRight)
            {
                GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
            }
            if (!canMoveLeft)
            {
                if (h > 0)
                {
                    GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
                }
            }
            if (!canMoveRight)
            {
                if (h < 0)
                {
                    GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
                }
            }
        }

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !facingRight)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && facingRight)
            // ... flip the player.
            Flip();

        // If the player should jump...
        if (jumpable)
        {
            // Set the Jump animator trigger parameter.
            //anim.SetTrigger("Jump");

            // Add a vertical force to the player.
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

            source.PlayOneShot(jumpSound);

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jumpable = false;
        }
    }


    void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "IceBlock-Ground")
        {
            onIce = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "IceBlock-Ground")
        {
            onIce = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "LBorder")
        {
            canMoveLeft = false;
        }

        if (collision.gameObject.tag == "RBorder")
        {
            canMoveRight = false;
        }

        if (collision.gameObject.tag == "UBorder")
        {
            canMoveUp = false;
        }

        if (collision.gameObject.tag == "Checkpoint")
        {
            if (name == "Mage")
            {
                respawnPoint = collision.transform.position + respawnOffset;
            }

            if (name == "Warrior")
            {
                respawnPoint = collision.transform.position - respawnOffset;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "LBorder")
        {
            canMoveLeft = true;
        }

        if (collision.gameObject.tag == "RBorder")
        {
            canMoveRight = true;
        }

        if (collision.gameObject.tag == "UBorder")
        {
            canMoveUp = true;
        }
    }

    public void RespawnWithOffset()
    {
        if (name == "Mage")
        {
            transform.position = respawnPoint - respawnOffset;
            GetComponent<Health>().ResetHealth();
        }

        if (name == "Warrior")
        {
            transform.position = respawnPoint + respawnOffset;
            GetComponent<Health>().ResetHealth();
        }
    }

    public void SetCheckpoint(Vector3 checkedPoint)
    {
        respawnPoint = checkedPoint;
    }
}