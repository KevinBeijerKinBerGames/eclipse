﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Effect of the push and pull magic.
/// </summary>
public class KnockBack : MonoBehaviour
{
    [SerializeField]
    float magnitude = 1;

    // public int health = 3;
	
    PlayerMovement movement;

	public bool bashed = false;
	//public bool enemyBashed = false;

	void Start()
    {
        magnitude = magnitude * 1000;
        movement = GetComponent<PlayerMovement>();

		if (name == "Warrior")
		{
			Physics2D.IgnoreLayerCollision(12, 11);
		}

		if (name == "Mage")
		{
			Physics2D.IgnoreLayerCollision(9, 10);
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
        //var direction = transform.position - collision.transform.position;
        var upForce = transform.up;
        var directionForce = transform.right;

        if (collision.gameObject.tag == "Push")
        {
            if (collision.gameObject.transform.rotation.eulerAngles.y > -181 || collision.gameObject.transform.rotation.eulerAngles.y < -179)
            {
                directionForce = -transform.right;
            }
            if (collision.gameObject.transform.rotation.eulerAngles.y == 0)
            {
                directionForce = transform.right;
            }
        }
        if (collision.gameObject.tag == "Pull")
        {
            if (collision.gameObject.transform.rotation.eulerAngles.y > -181 || collision.gameObject.transform.rotation.eulerAngles.y < -179)
            {
                directionForce = transform.right;
            }
            if (collision.gameObject.transform.rotation.eulerAngles.y == 0)
            {
                directionForce = -transform.right;
            }
        }

        if (tag == "Enemy")
        {
            if (collision.gameObject.tag == "FireBall")
            {
                GetComponentInParent<EnemyHealth>().HealthChange(3);
            }

            if (collision.gameObject.tag == "Push")
            {
                transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude);
                transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(upForce * magnitude);
            }

            if (collision.gameObject.tag == "Pull")
            {
                transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude);
                transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(upForce * magnitude);
            }
        }
        if (name == "Warrior")
        {
            if (collision.gameObject.tag == "Push")
            {
                GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude);
                GetComponent<Rigidbody2D>().AddForce(upForce * magnitude / 5);
                movement.boost = true;
            }

            if (collision.gameObject.tag == "Pull")
            {
                GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude);
                GetComponent<Rigidbody2D>().AddForce(upForce * magnitude / 5);
                movement.boost = true;
            }
        }
    }

	void OnTriggerStay2D(Collider2D collision)
	{
        //var direction = transform.position - collision.transform.position;
        var upForce = transform.up;

		if (name == "Mage")
		{
			if (collision.gameObject.tag == "Launching")
			{
                GetComponent<Rigidbody2D>().AddForce(upForce * magnitude / 5);
            }
		}
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
        //var direction = transform.position - collision.transform.position;
        var upForce = transform.up;
        var directionForce = transform.right;

        if (collision.gameObject.tag == "Shield")
        {
            if (collision.gameObject.transform.rotation.eulerAngles.y > -181 || collision.gameObject.transform.rotation.eulerAngles.y < -179)
            {
                directionForce = -transform.right;
            }
            if (collision.gameObject.transform.rotation.eulerAngles.y == 0)
            {
                directionForce = transform.right;
            }
        }

        if (tag == "Enemy")
		{
			if (collision.gameObject.tag == "ShieldPush")
			{
				GetComponentInParent<EnemyHealth>().HealthChange(1);
				
				directionForce.Normalize();
				transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude * 1.5f);
				transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(upForce * magnitude);
			}
		}

		if (name == "Mage")
		{
			if (collision.gameObject.tag == "ShieldPush")
			{
				directionForce.Normalize();
				GetComponent<Rigidbody2D>().AddForce(directionForce * magnitude / 1.5f);
				GetComponent<Rigidbody2D>().AddForce(upForce * magnitude / 3.5f);
			}

			/*if (collision.gameObject.tag == "Launching")
			{
				Debug.Log("Trigger Launch");
				upForce.Normalize();
				GetComponent<Rigidbody2D>().AddForce(upForce * magnitude / 3);
			}*/
		}
	}
}