﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script to fire the push, pull, fire, and ice magic.
/// </summary>
public class Magic : MonoBehaviour
{
    GameObject stats;
    ControllerCheck check;

	public GameObject pull;
	public GameObject push;
	public GameObject fireBall;
	public GameObject iceBlock;

	public Transform pushSpawn;
    public Transform pullSpawn;
	public Transform iceSpawn;
	//If you want to make a unique spawnpoint for the fireball.
	//public Transform ballSpawn;

	public float shotRate = 2.0f;
    public float fireRate = 3.0f;
	public float iceRate = 6.5f;
	private float nextShot = 0.0f;
    private float fireShot = 0.0f;
    private float iceShot = 0.0f;

    [SerializeField]
    string pushCommand = "Fire1_P1", pullCommand = "Fire2_P1", fireCommand = "Fire3_P1", iceCommand = "Fire4_P1";

	PlayerMovement playerMovement;

    [SerializeField]
    AudioClip wind, fire, ice;

    private readonly float volHigh = 0.3f;
    private readonly float volLow = 0.2f;

    AudioSource source;

    [SerializeField]
    GameObject windAnimatorObject, fireAnimatorObject, iceAnimatorObject;
    Animator windAnimator, fireAnimator, iceAnimator;
    
//    AnalyticsManager analytics;

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        source = GetComponent<AudioSource>();

        windAnimator = windAnimatorObject.GetComponent<Animator>();
        fireAnimator = fireAnimatorObject.GetComponent<Animator>();
        iceAnimator = iceAnimatorObject.GetComponent<Animator>();

//        analytics = stats.GetComponent<AnalyticsManager>();
    }

    void Start()
    {
        if (check.player1_PS4)
        {
            pushCommand = "Fire1_P1_PS4";
            pullCommand = "Fire2_P1_PS4";
            fireCommand = "Fire3_P1_PS4";
            iceCommand = "Fire4_P1_PS4";
        }
        else if (check.player1_XBox)
        {
            pushCommand = "Fire1_P1_XBox";
            pullCommand = "Fire2_P1_XBox";
            fireCommand = "Fire3_P1_XBox";
            iceCommand = "Fire4_P1_XBox";
        }
        else
        {
            pushCommand = "Fire1_P1_Keyboard";
            pullCommand = "Fire2_P1_Keyboard";
            fireCommand = "Fire3_P1_Keyboard";
            iceCommand = "Fire4_P1_Keyboard";
        }
    }

    void Update ()
	{
        float vol = Random.Range(volLow, volHigh);

        if (Input.GetButton(pushCommand) && Time.time > nextShot)
		{
			nextShot = Time.time + shotRate;

            if (!windAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
            {
                windAnimator.SetTrigger("Active");
            }

            FirePush(push);

            source.PlayOneShot(wind, vol);
        }

		if (Input.GetButton(pullCommand) && Time.time > nextShot)
		{
			nextShot = Time.time + shotRate;

            if (!windAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
            {
                windAnimator.SetTrigger("Active");
            }
            FirePull(pull);

            source.PlayOneShot(wind, vol);
        }

        if (check.player1_PS4 || check.player1_XBox)
        {
            float fireCheck = Input.GetAxis(fireCommand);
            if (fireCheck > 0 && Time.time > fireShot)
            {
                fireShot = Time.time + fireRate;

                if (!fireAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                {
                    fireAnimator.SetTrigger("Active");
                }

                FireFire(fireBall);

                source.PlayOneShot(fire, vol);
            }
        }
        else
        {
            if (Input.GetButtonDown(fireCommand) && Time.time > fireShot)
            {
                fireShot = Time.time + fireRate;

                if (!fireAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                {
                    fireAnimator.SetTrigger("Active");
                }

                FireFire(fireBall);

                source.PlayOneShot(fire, vol);
            }
        }


        if (check.player1_PS4 || check.player1_XBox)
        {
            float iceCheck = Input.GetAxis(iceCommand);
            if (iceCheck > 0 && Time.time > iceShot)
            {
                iceShot = Time.time + iceRate;

                if (!iceAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                {
                    iceAnimator.SetTrigger("Active");
                }

                FireIce(iceBlock);

                source.PlayOneShot(ice, vol*2);
            }
        }    
        else
        {
            if (Input.GetButtonDown(iceCommand) && Time.time > iceShot)
            {
                iceShot = Time.time + iceRate;

                if (!iceAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cooldown"))
                {
                    iceAnimator.SetTrigger("Active");
                }

                FireIce(iceBlock);

                source.PlayOneShot(ice, vol*2);
            }
        }
    }

    // Why are there different fire functions here while they all do the same thing; Can't we make the instantiate with an if statement that checks what Gameobject is being used.

    void FirePull(GameObject bullet)
    {
        if (playerMovement.facingRight)
        {
			pullSpawn.rotation = new Quaternion(0,0,0,0);
            Instantiate(bullet, pullSpawn.position, pullSpawn.rotation);
		}
        else
        {
			pullSpawn.rotation = new Quaternion(0,180,0,0);
			Instantiate(bullet, pullSpawn.position, pullSpawn.rotation);
		}

//        analytics.pullAmount++;
    }

	void FirePush(GameObject bullet)
	{
		if (playerMovement.facingRight)
		{
			pushSpawn.rotation = new Quaternion(0, 0, 0, 0);
			Instantiate(bullet, pushSpawn.position, pushSpawn.rotation);
		}
		else
		{
			pushSpawn.rotation = new Quaternion(0, 180, 0, 0);
			Instantiate(bullet, pushSpawn.position, pushSpawn.rotation);
		}

//        analytics.pushAmount++;
    }

	void FireFire(GameObject bullet)
	{
		if (playerMovement.facingRight)
		{
			pushSpawn.rotation = new Quaternion(0, 0, 0, 0);
			Instantiate(bullet, pushSpawn.position, pushSpawn.rotation);
		}
		else
		{
			pushSpawn.rotation = new Quaternion(0, 180, 0, 0);
			Instantiate(bullet, pushSpawn.position, pushSpawn.rotation);
		}

//        analytics.fireAmount++;
    }

	void FireIce(GameObject bullet)
	{
		if (playerMovement.facingRight)
		{
			iceSpawn.rotation = new Quaternion(0, 0, 0, 0);
			Instantiate(bullet, iceSpawn.position, iceSpawn.rotation);
		}
		else
		{
			iceSpawn.rotation = new Quaternion(0, 180, 0, 0);
			Instantiate(bullet, iceSpawn.position, iceSpawn.rotation);
		}

//        analytics.iceAmount++;
    }
}