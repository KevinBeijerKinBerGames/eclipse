﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for the movement of the push and pull and fire magic.
/// </summary>
public class Pew : MonoBehaviour
{
	Rigidbody2D rb;

	public float speed;

    CircleCollider2D push;

	float destroyTime = 0.0f;

    Animator animator;
    bool dying;

	void Awake()
	{
		destroyTime = 0.0f;

        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        push = GetComponent<CircleCollider2D>();
    }

	void Start()
	{
		rb.velocity = transform.right * speed;
	}

    void Update()
    {
        destroyTime += Time.deltaTime;

        if (tag == "FireBall")
        {
            if (destroyTime >= 2.0f)
            {
                Destroy(gameObject);
            }
        }

        if ((tag == "Push" || tag == "Pull") && destroyTime >= 2.0f)
        {
            animator.SetTrigger("Break");
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Break") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Push" || collision.gameObject.tag == "Pull" || collision.gameObject.name == "IceCube")
        {
            if (this.tag == "FireBall")
            {
                Destroy(gameObject);
            }
            push.enabled = false;
            animator.SetTrigger("Break");
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PTrigger" || collision.gameObject.tag == "Shield")
        {
            if (this.tag == "FireBall")
            {
                Destroy(gameObject);
            }
            push.enabled = false;
            animator.SetTrigger("Break");
        }
    }
}
