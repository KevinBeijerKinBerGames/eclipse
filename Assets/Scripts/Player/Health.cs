﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Slider wHealthBar;
    public Slider mHealthBar;

    public static int wCurHealth;
    public static int mCurHealth;

    int wInitialHealth;
    int mInitialHealth;

    public GameObject spawner;

    public AudioClip DEATH;

    private AudioSource source;

    GameObject platformMoverObject;
    PlatformMover mover;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        platformMoverObject = GameObject.Find("Ship Manager");
        mover = platformMoverObject.GetComponent<PlatformMover>();
    }

    void Start()
    {
        wCurHealth = 15;
        mCurHealth = 10;

        wInitialHealth = wCurHealth;
        mInitialHealth = mCurHealth;
    }

    void Update()
    {
        wHealthBar.value = wCurHealth;
        mHealthBar.value = mCurHealth;

        if (wCurHealth <= 0 || mCurHealth <= 0)
        {
            source.PlayOneShot(DEATH);
            spawner.GetComponent<PlayerRespawn>().RespawnCharacters();
        }
    }

    public void ResetHealth()
    {
        wCurHealth = wInitialHealth;
        mCurHealth = mInitialHealth;

        mover.Reset();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (name == "Warrior")
        {
            if (collision.gameObject.tag == "Enemy")
            {
                if (wCurHealth > 0)
                {
                    wCurHealth = wCurHealth - 2;
                }
            }
            if (collision.gameObject.tag == "Explosion")
            {
                if (wCurHealth > 0)
                {
                    wCurHealth = wCurHealth - 3;
                }
            }
            if (collision.gameObject.tag == "Death")
            {
                wCurHealth = 0;
            }
        }

        if (name == "Mage")
        {
            if (collision.gameObject.tag == "Enemy")
            {
                if (mCurHealth > 0)
                {
                    mCurHealth = mCurHealth - 2;
                }
            }
            if (collision.gameObject.tag == "Explosion")
            {
                if (mCurHealth > 0)
                {
                    mCurHealth = mCurHealth - 3;
                }
            }
            if (collision.gameObject.tag == "Death")
            {
                mCurHealth = 0;
            }
        }
    }

    public void GiveBack()
    {
        wCurHealth = wCurHealth + 2;
    }
}