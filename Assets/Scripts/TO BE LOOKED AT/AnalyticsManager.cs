﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour
{
    public int pushAmount, pullAmount, fireAmount, iceAmount, bashAmount, launchAmount;

    public bool send;

    AnalyticsResult result;

    private Dictionary<string, object> parameters = new Dictionary<string, object>();

    void Awake ()
    {
        pushAmount = pullAmount = fireAmount = iceAmount = bashAmount = launchAmount = 0;
        send = false;
	}

    private void Update()
    {
        if (send)
        {
            Dispatch();
            send = false;
        }
    }

    public bool Dispatch()
    {
        parameters["Push"] = pushAmount;
        parameters["Pull"] = pullAmount;
        parameters["Fire"] = fireAmount;
        parameters["Ice"] = iceAmount;
        parameters["ShieldBash"] = bashAmount;
        parameters["ShieldLaunch"] = launchAmount;

        result = AnalyticsEvent.Custom("Skills used", parameters);

        if (result == AnalyticsResult.Ok)
        {
            Debug.Log("Skill Data Sent");
            return true;
        }
        else
        {
            Debug.Log("Skill Analytics Error");
            Debug.Log("Parameters length: " + parameters.Count);
            return false;
        }
    }
}