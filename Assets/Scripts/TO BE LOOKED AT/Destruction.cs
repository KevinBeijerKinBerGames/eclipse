﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Should probably rename this but it will clash with the Cutscenes script.
/// It checks if the animation is done playing and then maybe an action.
/// </summary>
public class Destruction : MonoBehaviour
{
    Animator animator;

    SpriteRenderer spriteRenderer;

    [SerializeField]
    Sprite sprite;

	void Awake ()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("End"))
        {
            spriteRenderer.sprite = sprite;

            if (tag == "Potion")
            {
                SceneManager.LoadScene("Level 1 - Forest");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            animator.SetTrigger("Start");
        }
    }
}
