﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour
{
    [SerializeField]
    GameObject textObject_1, textObject_2;

    GameObject statsObject;
    Stats stats;
    ControllerCheck check;
    string startCommand;
    int cutscene;

    void Awake ()
    {
        statsObject = GameObject.FindGameObjectWithTag("Stats");
        stats = statsObject.GetComponent<Stats>();
        check = stats.GetComponent<ControllerCheck>();
        cutscene = stats.cutsceneCheck;

        if (cutscene == 0)
        {
            textObject_1.SetActive(true);
        }
        if (cutscene == 1)
        {
            textObject_2.SetActive(true);
        }
    }
	
	void Update ()
    {
        if (check.player1_PS4)
        {
            startCommand = "Start_P1_PS4";
        }
        else if (check.player1_XBox)
        {
            startCommand = "Start_P1_XBox";
        }
        else
        {
            startCommand = "Start_P1_Keyboard";
        }

        if (Input.GetButtonDown(startCommand))
        {
            CancelInvoke();
            Continue();
        }

        Invoke("Continue", 25);
	}

    void Continue()
    {
        stats.cutsceneCheck++;
        if (cutscene == 0)
        {
            SceneManager.LoadScene("Level 0 - Tutorial");
        }
        else
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
