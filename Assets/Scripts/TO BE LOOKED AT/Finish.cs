﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// To be deleted script that finished the 1st level as well as the final cutscene.
/// </summary>
public class Finish : MonoBehaviour
{
    private void Update()
    {
        if (tag == "End")
        {
            Invoke("End", 5);
        }
        if (tag == "Disclaimer")
        {
            Invoke("Begin", 15);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SceneManager.LoadScene("Cutscene");
        }
    }

    void Begin()
    {
        SceneManager.LoadScene("Tutorial_CoOp");
    }

    void End()
    {
        SceneManager.LoadScene("Menu");
    }
}