﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{
    [TextArea (1,10)]
    public string[] texts;
    [SerializeField]
    int[] cutsceneOrder;

    GameObject texter1, texter2;
    PlayerText text1, text2;

    int cut = 0;

    bool run = false, running = false;

    void Awake()
    {
        texter1 = GameObject.Find("Player 1 Canvas");
        text1 = texter1.GetComponent<PlayerText>();
        texter2 = GameObject.Find("Player 2 Canvas");
        text2 = texter2.GetComponent<PlayerText>();
    }

    void Update()
    {
        if (run)
        {
            if (!running)
            {
                Invoke("SendText", 3);
                running = true;
            }
        }
           
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (!run && texts.Length != 0)
            {
                SendText();
                run = true;
            }
        }
    }

    void SendText()
    {
        if (cutsceneOrder[cut] == 1)
        {
            text1.ShowText(texts[cut]);
        }
        if (cutsceneOrder[cut] == 2)
        {
            text2.ShowText(texts[cut]);
        }

        if (cut != texts.Length)
        {
            cut++;
            running = false;
        }
    }
}
