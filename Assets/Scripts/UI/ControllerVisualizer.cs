﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Visualizes what controller settings are being used. (Under the title in the menu)
/// </summary>
public class ControllerVisualizer : MonoBehaviour
{
    GameObject textObject;
    Text text;

    ControllerCheck check;

    string p1, p2;

	void Awake ()
    {
        check = GetComponent<ControllerCheck>();
	}

    void Update ()
    {
        textObject = GameObject.FindGameObjectWithTag("ControllerCheck");
        if (textObject != null)
        {
            text = textObject.GetComponent<Text>();
        }

        if (check.player1_PS4)
        {
            p1 = "PS4";
        }
        else if (check.player1_XBox)
        {
            p1 = "XBox";
        }
        else
        {
            p1 = "Keyboard";
        }

        if (check.player2_PS4)
        {
            p2 = "PS4";
        }
        else if (check.player2_XBox)
        {
            p2 = "XBox";
        }
        else
        {
            p2 = "Keyboard";
        }

        if (textObject != null)
        {
            text.text = "player 1 uses " + p1 + ", player 2 uses " + p2;
        }
    }
}
