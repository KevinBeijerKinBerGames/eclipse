﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerText : MonoBehaviour
{
    Text thisText;
    PlayerMovement movement;
    RectTransform rTransform;

	void Awake ()
    {
        thisText = GetComponentInChildren<Text>();
        movement = GetComponentInParent<PlayerMovement>();
        rTransform = GetComponent<RectTransform>();

        thisText.text = "";
	}
	
	void Update ()
    {
        if (movement.facingRight)
        {
            rTransform.SetPositionAndRotation(rTransform.position, new Quaternion(0,0,0, 0));
        }
        else
        {
            rTransform.SetPositionAndRotation(rTransform.position, new Quaternion(0, 180, 0, 0));
        }
	}

    public void ShowText(string sentence)
    {
        thisText.text = sentence;

        Invoke("RemoveText", 3);
    }

    void RemoveText()
    {
        thisText.text = "";
    }
}
