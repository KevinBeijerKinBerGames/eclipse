﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Script for the multiple UI buttons.
/// </summary>
public class ButtonFunctions : MonoBehaviour
{
    float switchDelay = 0.0f;

    string sceneName;

    bool hasPressed = false;

    int specifiedFunction = 0;

    GameObject statsObject;
    Stats stats;
    
    void Awake()
    {
        statsObject = GameObject.FindGameObjectWithTag("Stats");
        stats = statsObject.GetComponent<Stats>();
    }

    public void MenuButton()
    {
        sceneName = "Menu";
        stats.cutsceneCheck = 0;
        specifiedFunction = 1;
        StartCoroutine("Delay");
    }

    public void Play2P2CButton()
    {
        sceneName = "Cutscene";
        specifiedFunction = 1;
        StartCoroutine("Delay");
    }

    public void SettingsButton()
    {
        sceneName = "Settings";
        specifiedFunction = 1;
        StartCoroutine("Delay");
    }

    public void CreditsButton()
    {
        sceneName = "Credits";
        specifiedFunction = 1;
        StartCoroutine("Delay");
    }

    public void QuitButton()
    {
        specifiedFunction = 2;
        StartCoroutine("Delay");
    }

    //Coroutine to delay the switching of scenes to allow the button audio to play completely.
    IEnumerator Delay()
    {
        while (true)
        {
            switchDelay += Time.deltaTime;

            if (switchDelay >= 0.1f)
            {
                hasPressed = true;

                if (specifiedFunction == 1)
                {
                    SceneManager.LoadScene(sceneName);
                }

                if (specifiedFunction == 2)
                {
#if UNITY_EDITOR
                    EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
                }
            }

            if (hasPressed == true)
            {
                StopAllCoroutines();
            }

            yield return new WaitForEndOfFrame();
        }
    }
}