﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Pop-up menu for during the levels.
/// </summary>
public class Options : MonoBehaviour
{
    GameObject menu;

    bool active;

    GameObject stats;
    ControllerCheck check;
    string startCommand;

    [SerializeField]
    int player = 1;

    GameObject close;
    Button closeButton;

    GameObject[] players;
    GameObject player1, player2;
    PlayerMovement movementScript1, movementScript2;

    AnalyticsManager analytics;

    void Start ()
	{
        menu = GameObject.FindGameObjectWithTag("Menu");
        menu.SetActive(false);

        close = menu.transform.GetChild(3).gameObject;
        closeButton = close.GetComponent<Button>();

        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();

        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            player1 = players[0];
            player2 = players[1];
            movementScript1 = player1.GetComponent<PlayerMovement>();
            movementScript2 = player2.GetComponent<PlayerMovement>();
        }
        else
        {
            Debug.Log("Player error: Are there really 2 players in the game?");
        }

        analytics = stats.GetComponent<AnalyticsManager>();
    }
	
	void Update ()
	{
        if (player == 1)
        {
            if (check.player1_PS4)
            {
                startCommand = "Start_P1_PS4";
            }
            else if (check.player1_XBox)
            {
                startCommand = "Start_P1_XBox";
            }
            else
            {
                startCommand = "Start_P1_Keyboard";
            }
        }
        if (player == 2)
        {
            if (check.player2_PS4)
            {
                startCommand = "Start_P2_PS4";
            }
            else if (check.player2_XBox)
            {
                startCommand = "Start_P2_XBox";
            }
            else
            {
                startCommand = "Start_P2_Keyboard";
            }
        }

        if (Input.GetButtonDown(startCommand))
        {
            if (active)
            {
                Close();
            }
            else
            {
                Open();
                analytics.send = true;
            }
        }
	}

    public void Open()
    {
        menu.SetActive(true);
        active = true;
        closeButton.Select();

        if (movementScript1 != null && movementScript2 != null)
        {
            movementScript1.enabled = false;
            movementScript2.enabled = false;
        }
    }

    public void Close()
    {
        menu.SetActive(false);
        active = false;

        if (movementScript1 != null && movementScript2 != null)
        {

            movementScript1.enabled = true;
            movementScript2.enabled = true;
        }
    }
}