﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script that makes it visible which buttons are being pressed.
/// - Will be deleted (or used in a different way) in later versions.
/// </summary>
public class Buttons : MonoBehaviour
{
    [SerializeField]
    Sprite keyboardSprite, xBoxSprite, pS4Sprite;

    GameObject stats;
    ControllerCheck check;
    SpriteRenderer render;

    [SerializeField]
    int player = 1;

    void Awake()
    {
        stats = GameObject.FindGameObjectWithTag("Stats");
        check = stats.GetComponent<ControllerCheck>();
        render = GetComponent<SpriteRenderer>();
    }
    
    void Update ()
	{
        if (player == 1)
        {
            if (check.player1_XBox)
            {
                if (xBoxSprite != null)
                {
                    render.sprite = xBoxSprite;
                }
            }
            else if (check.player1_PS4)
            {
                if (pS4Sprite != null)
                {
                    render.sprite = pS4Sprite;
                }
            }
            else
            {
                if (keyboardSprite != null)
                {
                    render.sprite = keyboardSprite;
                }
            }
        }
        if (player == 2)
        {
            if (check.player2_XBox)
            {
                if (xBoxSprite != null)
                {
                    render.sprite = xBoxSprite;
                }
            }
            else if (check.player2_PS4)
            {
                if (pS4Sprite != null)
                {
                    render.sprite = pS4Sprite;
                }
            }
            else
            {
                if (keyboardSprite != null)
                {
                    render.sprite = keyboardSprite;
                }
            }
        }
    }
}