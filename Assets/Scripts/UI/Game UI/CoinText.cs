﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Text for the amount of coins.
/// </summary>
public class CoinText : MonoBehaviour
{
    public int amount = 0;

    Text coinText;

    GameObject statsObject;

    Stats stats;

	void Start ()
	{
        coinText = GetComponent<Text>();
        statsObject = GameObject.FindGameObjectWithTag("Stats");
        stats = statsObject.GetComponent<Stats>();
        amount = stats.coins;
	}
	
	void Update ()
	{
        if (amount > 99999)
        {
            amount = 99999;
        }
        coinText.text = "Coins: " + amount;
        stats.coins = amount;
	}
}