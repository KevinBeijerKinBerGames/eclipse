﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for all stats to be saved.
/// Toxicity and gold amount for now.
/// </summary>
public class Stats : MonoBehaviour
{
    public static Stats instance = null;

    public int coins;

    public int cutsceneCheck = 0;

	void Start ()
	{
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
	}

    private void Update()
    {
        if (coins < 0)
        {
            coins = 0;
        }
        if (coins > 99999)
        {
            coins = 99999;
        }
    }
}