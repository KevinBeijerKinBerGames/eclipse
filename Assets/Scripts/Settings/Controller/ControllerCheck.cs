﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script that checks, what kind of controllers are plugged in.
/// </summary>
public class ControllerCheck : MonoBehaviour
{
    public bool player1_XBox = false, player1_PS4 = false, player2_XBox = false, player2_PS4 = false;

    int player = 1;

    public bool overRide = false;
	
	void Update ()
	{
        if (!overRide)
        {
            // Gets 1 or 2 controllers
            string[] names = Input.GetJoystickNames();

            // For every controller detected, get a x.
            for (int x = 0; x < names.Length; x++)
            {
                if (names[x].Length == 19)
                {
                    if (x == 0)
                    {
                        player1_PS4 = true;
                        player1_XBox = false;
                    }
                    if (x == 1)
                    {
                        player2_PS4 = true;
                        player2_XBox = false;
                    }
                }
                else if (names[x].Length == 33)
                {
                    if (x == 0)
                    {
                        player1_XBox = true;
                        player1_PS4 = false;
                    }
                    if (x == 1)
                    {
                        player2_XBox = true;
                        player2_PS4 = false;
                    }
                }
            }
        }
    }
}