﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script that overrides the automatic controller settings.
/// </summary>
public class ControllerOverride : MonoBehaviour
{
    GameObject controllerObject;
    ControllerCheck controllerCheck;
    
    [SerializeField]
    Dropdown player1, player2;

    void Awake()
    {
        controllerObject = GameObject.FindGameObjectWithTag("Stats");
        controllerCheck = controllerObject.GetComponent<ControllerCheck>();
    }

    void Start ()
    {
        if (controllerCheck.player1_PS4)
        {
            player1.value = 1;
        }
        else if (controllerCheck.player1_XBox)
        {
            player1.value = 2;
        }
        else
        {
            player1.value = 0;
        }

        if (controllerCheck.player2_PS4)
        {
            player2.value = 1;
        }
        else if (controllerCheck.player2_XBox)
        {
            player2.value = 2;
        }
        else
        {
            player2.value = 0;
        }
    }

    public void Change1()
    {
        if (player1.value == 0)
        {
            controllerCheck.player1_PS4 = false;
            controllerCheck.player1_XBox = false;
        }
        if (player1.value == 1)
        {
            controllerCheck.player1_PS4 = true;
            controllerCheck.player1_XBox = false;
        }
        if (player1.value == 2)
        {
            controllerCheck.player1_PS4 = false;
            controllerCheck.player1_XBox = true;
        }
        controllerCheck.overRide = true;
    }

    public void Change2()
    {
        if (player2.value == 0)
        {
            controllerCheck.player2_PS4 = false;
            controllerCheck.player2_XBox = false;
        }
        if (player2.value == 1)
        {
            controllerCheck.player2_PS4 = true;
            controllerCheck.player2_XBox = false;
        }
        if (player2.value == 2)
        {
            controllerCheck.player2_PS4 = false;
            controllerCheck.player2_XBox = true;
        }
        controllerCheck.overRide = true;
    }
}
