﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTime : MonoBehaviour
{
    [SerializeField]
    float lifeTime = 0;

	void Update ()
    {
        Destroy(this.gameObject, lifeTime);
    }
}
