﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script that checks for the gamemodes.
/// </summary>
public class ModeCheck : MonoBehaviour
{
    Scene currentScene;

    public bool characters = false, coOp = false, shieldSurfing = false;

	void Start ()
	{
        currentScene = SceneManager.GetActiveScene();
	}
	
	void Update ()
	{
        if (currentScene.name == "Level 0 - Tutorial" || currentScene.name == "Level 1 - Forest")
        {
            characters = true;
            coOp = false;
        }
        if (currentScene.name == "Tutorial_CoOp")
        {
            coOp = true;
            characters = false;
        }
	}
}