﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// The levels are build from "areas" that are loaded in whenever the player gets close.
/// They also get destroyed when the player leaves, this saves computational power and makes sure the enemies don't wander off.
/// </summary>
public class LoadArea : MonoBehaviour
{
    [SerializeField]
    GameObject area;

    private GameObject placedArea;
    private int players = 0;

//    private Dictionary<string, object> parameters = new Dictionary<string, object>();

/*
    private void Start()
    {
        parameters.Add("area", "Unknown");
        parameters.Add("goal", "Unknown");
    }

    public bool Dispatch(string area, string goal)
    {
        parameters["area"] = area;
        parameters["goal"] = goal;

        AnalyticsResult result;

        result = AnalyticsEvent.Custom("AreaUsage", parameters);

        if (result == AnalyticsResult.Ok)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
*/

    private void Update()
    {
        if (players > 0 && placedArea == null)
        {
            placedArea = Instantiate(area, transform.position, transform.rotation);
            placedArea.transform.parent = transform;

//            Dispatch(area.name, "Enter");
        }
        if (players == 0 && placedArea != null)
        {      
            Destroy(placedArea);

//            Dispatch(area.name, "Exit");
        }
    }

    private void OnTriggerEnter2D(Collider2D collisionIn)
    {
        if (collisionIn.tag == "Player")
        {
            players++;
        }
    }

    private void OnTriggerExit2D(Collider2D collisionOut)
    {
        if (collisionOut.tag == "Player")
        {
            players--;
        }
    }
}