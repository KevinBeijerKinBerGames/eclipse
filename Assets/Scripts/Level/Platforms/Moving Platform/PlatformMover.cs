﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that moves a platform between a start and end point.
/// (Attach to platform manager, with 3 children, in order: platform, start position, end position)
/// </summary>
public class PlatformMover : MonoBehaviour
{
    // You know what these mean.
    public Transform movingPlatform;
    public Transform firstPosition;
    public Transform secondPosition;
    public Transform thirdPosition;
    public Transform fourthPosition;

    public bool sway = false;

    // Position to move to. (public for debug purposes)
    public Vector2 newPosition;

    // Current state the platform is in. (public for debug purposes)
    public string currentState;

    // Smoothness of movement. (* Time.deltaTime)
    public float smooth;

    // Reset timer, how long before changing state.
    public float resetTime;

    // If changing target = true, if done changing = false.
    bool changing;

    /// <summary>
    /// Gets the variables.
    /// </summary>
	void Start ()
	{
        changing = false;
        movingPlatform = transform.GetChild(0);
        firstPosition = transform.GetChild(1);
        secondPosition = transform.GetChild(2);

        if (transform.childCount > 3)
        {
            thirdPosition = transform.GetChild(3);
            if (transform.childCount > 4)
            {
                fourthPosition = transform.GetChild(4);
            }
        }

        ChangeTarget();
	}
	
    /// <summary>
    /// Moves platform towards target.
    /// If at target, invoke changing the target.
    /// </summary>
	void FixedUpdate ()
	{
        if (sway)
        {
            movingPlatform.position = Vector2.MoveTowards(movingPlatform.position, newPosition, smooth * Time.deltaTime);
        }
        
        if (movingPlatform.position == new Vector3(newPosition.x, newPosition.y, movingPlatform.transform.position.z) && !changing)
        {
            Invoke("ChangeTarget", resetTime);
            changing = true;
        }
    }

    /// <summary>
    /// Changes target.
    /// </summary>
    void ChangeTarget()
    {
        if (currentState == "Moving To First Position")
        {
            currentState = "Moving To Second Position";
            newPosition = secondPosition.position;
        }
        else if (currentState == "Moving To Second Position")
        {
            if (thirdPosition == null)
            {
                currentState = "Moving To First Position";
                newPosition = firstPosition.position;
            }
            else
            {
                currentState = "Moving to Third Position";
                newPosition = thirdPosition.position;
            }
        }
        else if (currentState == "Moving to Third Position")
        {
            if (fourthPosition == null)
            {
                currentState = "Moving To First Position";
                newPosition = firstPosition.position;
            }
            else
            {
                currentState = "Moving to Fourth Position";
                newPosition = fourthPosition.position;
            }
        }
        else if (currentState == "Moving to Fourth Position")
        {
            currentState = "Moving To First Position";
            newPosition = firstPosition.position;
        }
        else if (currentState == "")
        {
            currentState = "Moving To Second Position";
            newPosition = secondPosition.position;
        }
        changing = false;
    }

    public void Reset()
    {
        movingPlatform.position = firstPosition.position;
    }
}