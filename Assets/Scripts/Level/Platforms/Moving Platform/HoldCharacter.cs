﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that ties the player to the moving platform.
/// </summary>
public class HoldCharacter : MonoBehaviour
{
    PlatformMover platformMover;
    bool start;

    void Awake()
    {
        platformMover = transform.parent.parent.GetComponent<PlatformMover>();
        start = false;
    }

    void Update()
    {
        if (transform.childCount == 2)
        {
            start = true;
        }
        else
        {
            if (transform.childCount == 0 || (platformMover.movingPlatform.position.x == platformMover.newPosition.x && platformMover.movingPlatform.position.y == platformMover.newPosition.y))
            {
                start = false;
            }
        }

        if (start == true)
        {
            platformMover.sway = true;
        }
        else
        {
            platformMover.sway = false;
        }
    }

    /// <summary>
    /// When entering the platform, make the entering object a child of the platform collider.
    /// But only if that object isn't the ground.
    /// </summary>
    /// <param name="collider"> Entering object. </param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            collider.transform.parent = gameObject.transform;
        }
    }

    /// <summary>
    /// When leaving the platform, let the child go.
    /// But only if that object isn't the ground.
    /// </summary>
    /// <param name="collider"> Leaving object. </param>
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag != "ground")
        {
            collider.transform.parent = null;
        }

        GameObject warrior = GameObject.Find("Warrior");
        GameObject shield = GameObject.Find("Shield(Clone)");

        if (shield != null)
        {
            shield.transform.parent = warrior.transform;
        }
    }
}