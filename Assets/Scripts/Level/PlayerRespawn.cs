﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    Vector3 respawnPosition;

    public Transform mage;
    public Transform warrior;

    Vector3 respawnOffsetMage;
    Vector3 respawnOffsetWarrior;

    void Start()
    {
        respawnPosition = transform.position;
        respawnOffsetMage = new Vector3(-0.65f, 2, 0);
        respawnOffsetWarrior = new Vector3(0.65f, 2, 0);
    }

    public void RespawnCharacters()
    {
        mage.transform.position = gameObject.transform.position + respawnOffsetMage;
        warrior.transform.position = gameObject.transform.position + respawnOffsetWarrior;

        mage.GetComponent<Health>().ResetHealth();
        warrior.GetComponent<Health>().ResetHealth();
    }
}