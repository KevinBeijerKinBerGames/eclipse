﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Sprite active;
    public Sprite deactive;

    private SpriteRenderer checkpointSpriteRenderer;

    int inCheckpoint;

    public Transform spawner;

    [SerializeField]
    AudioClip checkpoint;

    AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    void Start()
    {
        checkpointSpriteRenderer = GetComponent<SpriteRenderer>();

        inCheckpoint = 0;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Mage" || collision.gameObject.name == "Warrior")
        {
            inCheckpoint++;

            if (inCheckpoint == 2)
            {
                spawner.transform.position = gameObject.transform.position;

                if (checkpointSpriteRenderer.sprite == deactive)
                {
                    source.PlayOneShot(checkpoint);
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Mage" || collision.gameObject.name == "Warrior")
        {
            inCheckpoint--;
        }

        if (collision.gameObject.name == "Spawner")
        {
            checkpointSpriteRenderer.sprite = deactive;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Spawner")
        {
            checkpointSpriteRenderer.sprite = active;
        }
    }
}