﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes the coins pickable, as well as adds the value to the text and so the stats.
/// </summary>
public class Coin : MonoBehaviour
{
    [SerializeField]
    GameObject coinText;

    CoinText coinTextScript;

    public int worth = 0;

    [SerializeField]
    AudioClip coin;

    AudioSource source;

    MeshRenderer render;

    bool picked = false;

    void Start()
    {
        coinText = GameObject.Find("Coin Text");
        coinTextScript = coinText.GetComponent<CoinText>();
        source = GetComponent<AudioSource>();
        render = GetComponent<MeshRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && picked == false)
        {
            coinTextScript.amount = coinTextScript.amount + worth;
            source.PlayOneShot(coin);
            picked = true;
            render.enabled = false;

            Invoke("Die", 0.5f);
        }
    }

    void Die()
    {
        Destroy(this.gameObject);
    }
}