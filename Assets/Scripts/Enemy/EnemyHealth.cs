﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Health of the enemy
/// </summary>
public class EnemyHealth : MonoBehaviour
{
    public int health = 9;

    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void HealthChange(int damageValue)
    {
        health = health - damageValue;
    }
}