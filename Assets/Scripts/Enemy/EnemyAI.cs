﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that keeps all other enemy scripts in check.
/// - Determines what kind of enemy it is and which scripts to use for that.
/// </summary>
public class EnemyAI : MonoBehaviour
{
    EnemyMovement movementScript;
    Charging chargingScript;
    Shooting shootingScript;
    Flying flyingScript;
    EnemyHealth healthScript;

    /*
     * Enemy types
     * 0 = nothing
     * 1 = walking
     * 2 = charging
     * 3 = shooting
     * 4 = flying
     */
    [Range(0 , 4)]
    [SerializeField]
    int enemyType;

    private void Awake()
    {
        movementScript = GetComponent<EnemyMovement>();
        chargingScript = GetComponent<Charging>();
        shootingScript = GetComponent<Shooting>();
        flyingScript = GetComponent<Flying>();
        healthScript = GetComponent<EnemyHealth>();
    }

    void Start()
    {
        if (enemyType == 0) // standing
        {
            movementScript.enabled = false;
            chargingScript.enabled = false;
            shootingScript.enabled = false;
            flyingScript.enabled = false;
        }
        if (enemyType == 1) // walking
        {
            movementScript.enabled = true;
            chargingScript.enabled = false;
            shootingScript.enabled = false;
            flyingScript.enabled = false;
        }
        if (enemyType == 2) // charging
        {
            movementScript.enabled = true;
            chargingScript.enabled = false;
            shootingScript.enabled = false;
            flyingScript.enabled = false;
        }
        if (enemyType == 3) // shooting
        {
            movementScript.enabled = false;
            chargingScript.enabled = false;
            shootingScript.enabled = true;
            flyingScript.enabled = false;
        }
        if (enemyType == 4) // flying
        {
            movementScript.enabled = false;
            chargingScript.enabled = false;
            shootingScript.enabled = false;
            flyingScript.enabled = true;
        }
    }

    private void Update()
    {
        if (enemyType == 2)
        {
            if (healthScript.health == 3)
            {
                chargingScript.enabled = true;
            }
        }
    }
}