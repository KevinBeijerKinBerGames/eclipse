﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for the enemy to be able to shoot (and combine with moving and flying)
/// </summary>
public class Shooting : MonoBehaviour
{
    Transform muzzle;

    [SerializeField]
    GameObject bullet;
    Bullet bulletAI;

    GameObject[] players;
    GameObject player1, player2, target;

    [SerializeField]
    float range, fireRate, delay;

    float distance1, distance2;

    bool firing = false;

    void Awake ()
	{
        muzzle = gameObject.transform.GetChild(0);

        players = GameObject.FindGameObjectsWithTag("Player");
    }
	
	void Update ()
	{
        if (players.Length == 2)
        {
            player1 = players[0];
            player2 = players[1];
            distance1 = Vector3.Distance(player1.transform.position, transform.position);
            distance2 = Vector3.Distance(player2.transform.position, transform.position);
        }
        else
        {
            player1 = players[0];
            player2 = null;
            distance1 = Vector3.Distance(player1.transform.position, transform.position);
            distance2 = Vector3.Distance(player1.transform.position, transform.position);
        }

        if (distance1 <= range)
        {
            if (!firing)
            {
                firing = true;
                target = player1;
                InvokeRepeating("Fire", delay, fireRate);
            }
        }
        if (distance2 <= range)
        {
            if (!firing)
            {
                firing = true;
                target = player2;
                InvokeRepeating("Fire", delay, fireRate);
            }
        }

        if ((distance1 > range && target == player1) || (distance2 > range && target == player2))
        {
            firing = false;
            CancelInvoke("Fire");
        }
    }

    void Fire()
    {
        if ((muzzle.position.x < transform.position.x && target.transform.position.x < transform.position.x) || (muzzle.position.x > transform.position.x && target.transform.position.x > transform.position.x))
        { 
            GameObject shot = Instantiate(bullet, muzzle.position, bullet.transform.rotation);
            shot.transform.SetParent(transform);

            bulletAI = shot.GetComponent<Bullet>();

            bulletAI.target = target;
        }
        else
        {
            CancelInvoke("Fire");
            firing = false;
        }
    }
}