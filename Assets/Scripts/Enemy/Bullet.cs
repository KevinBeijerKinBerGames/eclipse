﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for the bullets that are fired towards players.
/// </summary>
public class Bullet : MonoBehaviour
{
    [SerializeField]
    float speed;

    Rigidbody2D rb;

    public GameObject target;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.velocity = (target.transform.position - transform.position).normalized * speed;
    }
}