﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for flying enemies.
/// </summary>
public class Flying : MonoBehaviour
{
    Rigidbody2D rb;

    public float maxVelocity, acceleration, frequency;

    [SerializeField]
    float amplitude;

    float sqrMaxVelocity, startTime;

    Vector3 direction, orthogonal;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;

        SetMaxVelocity(maxVelocity);

        startTime = Time.time;
        direction = -transform.right;
        orthogonal = transform.up;
    }

    void FixedUpdate()
    {
        float t = Time.time - startTime;

        rb.velocity = direction * acceleration + orthogonal * amplitude * Mathf.Sin(frequency * t);

        var v = rb.velocity;
        if (v.sqrMagnitude > sqrMaxVelocity)
        {
            rb.velocity = v.normalized * maxVelocity;
        }
    }

    public void SetMaxVelocity(float maxVelocity)
    {
        this.maxVelocity = maxVelocity;
        sqrMaxVelocity = maxVelocity * maxVelocity;
    }
}