﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that allows enemies to suicide charge at players.
/// </summary>
public class Charging : MonoBehaviour
{
    EnemyMovement movementScript;

    [SerializeField]
    float range, chargeMaxvelocity, chargeAcceleration, chargeFrequency;

    float originMax, originAcceleration, originFrequency, distance1, distance2;

    GameObject[] players;
    GameObject player1, player2;

    Animator animator;

    bool charging = false;

	void Start ()
	{
        players = GameObject.FindGameObjectsWithTag("Player");

        movementScript = GetComponent<EnemyMovement>();

        if (movementScript.enabled)
        {
            originMax = movementScript.maxVelocity;
            originAcceleration = movementScript.acceleration;
        }

        animator = gameObject.GetComponent<Animator>();
    }
	
	void Update ()
	{
        if (players.Length == 2)
        {
            player1 = players[0];
            player2 = players[1];
            distance1 = Vector3.Distance(player1.transform.position, transform.position);
            distance2 = Vector3.Distance(player2.transform.position, transform.position);
        }
        else
        {
            player1 = players[0];
            player2 = null;
            distance1 = Vector3.Distance(player1.transform.position, transform.position);
            distance2 = Vector3.Distance(player1.transform.position, transform.position);
        }

        if (movementScript.enabled)
        {
            if (distance1 <= range)
            {
                if (movementScript.movingLeft && player1.transform.position.x < transform.position.x)
                {
                    movementScript.SetMaxVelocity(chargeMaxvelocity);
                    movementScript.acceleration = chargeAcceleration;
                    //animator.speed = 1.5f;
                }
                else if (movementScript.movingRight && player1.transform.position.x > transform.position.x)
                {
                    movementScript.SetMaxVelocity(chargeMaxvelocity);
                    movementScript.acceleration = chargeAcceleration;
                    //animator.speed = 1.5f;
                }
                else
                {
                    movementScript.TurnAround();
                }
            }
            if (distance2 <= range)
            {
                if (movementScript.movingLeft && player2.transform.position.x < transform.position.x)
                {
                    movementScript.SetMaxVelocity(chargeMaxvelocity);
                    movementScript.acceleration = chargeAcceleration;
                    //animator.speed = 1.5f;
                }
                else if (movementScript.movingRight && player2.transform.position.x > transform.position.x)
                {
                    movementScript.SetMaxVelocity(chargeMaxvelocity);
                    movementScript.acceleration = chargeAcceleration;
                    //animator.speed = 1.5f;
                }
                else
                {
                    movementScript.TurnAround();
                }
            }

            if (distance1 > range && distance2 > range)
            {
                movementScript.SetMaxVelocity(originMax);
                movementScript.acceleration = originAcceleration;
                //animator.speed = 1;
            }
        }
    }
}