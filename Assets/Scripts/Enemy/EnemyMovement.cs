﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for basic enemy movement
/// - Turns around when hitting objects or the end of the platform.
/// </summary>
public class EnemyMovement : MonoBehaviour
{
    Rigidbody2D rb;

    public float maxVelocity, acceleration;

    float sqrMaxVelocity;

    public bool movingLeft, movingRight;

    Animator animator;
    
    void Awake ()
	{
        SetMaxVelocity(maxVelocity);
        rb = GetComponent<Rigidbody2D>();

        movingLeft = true;
        movingRight = false;

        animator = gameObject.GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        var v = rb.velocity;
        if (v.sqrMagnitude > sqrMaxVelocity)
        {
            rb.velocity = v.normalized * maxVelocity;
        }
    }

    void Update ()
	{
        if (movingLeft)
        {
            rb.AddForce(new Vector3(-10, 0, 0) * acceleration);
            transform.rotation = new Quaternion(0, 0, 0, 0);
            animator.SetTrigger("Walking");
        }
        if (movingRight)
        {
            rb.AddForce(new Vector3(10, 0, 0) * acceleration);
            transform.rotation = new Quaternion(0, 180, 0, 0);
            animator.SetTrigger("Walking");
        }
    }

    public void SetMaxVelocity(float maxVelocity)
    {
        this.maxVelocity = maxVelocity;
        sqrMaxVelocity = maxVelocity * maxVelocity;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Ground")
        {
            rb.velocity = Vector2.zero;
            animator.ResetTrigger("Walking");
            //transform.Rotate(new Vector3(0, 180, 0));

            TurnAround();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle" || collision.tag == "Shield" || collision.tag == "Enemy")
        {
            TurnAround();
        }
    }

    public void TurnAround()
    {
        if (movingLeft)
        {
            movingLeft = false;
            movingRight = true;
        }
        else
        {
            movingLeft = true;
            movingRight = false;
        }
    }
}