﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    Charging charging;
    EnemyHealth health;

    [SerializeField]
    GameObject explosion;

    // Use this for initialization
    void Awake ()
    {
        charging = GetComponentInParent<Charging>();
        health = GetComponentInParent<EnemyHealth>();
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (charging.enabled)
            {
                Instantiate(explosion, transform.position, Quaternion.identity);
                health.HealthChange(9000);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (charging.enabled)
            {
                Instantiate(explosion, transform.position, Quaternion.identity);
                health.HealthChange(9000);
            }
        }
    }
}
